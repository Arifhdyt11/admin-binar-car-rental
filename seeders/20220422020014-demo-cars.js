"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    
    await queryInterface.bulkInsert("Cars", [
      {
        name: "Avanza",
        type: "Home Car",
        price: "450000",
        image: "avanza.png",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Ferrari",
        type: "Sport Car",
        price: "950000",
        image: "ferrari.jpg",
        size: "Medium",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: "Hummer",
        type: "Trail Car",
        price: "700000",
        image: "hummer.jpg",
        size: "Large",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {

    await queryInterface.bulkDelete("Cars", null, {});
    
  },
};
