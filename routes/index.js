const express = require("express");
const fs = require("fs");
const axios = require("axios").default;
const router = express.Router();
const multer = require("multer");
const path = require("path");

const publicDirectory = path.join(__dirname, "../public");

const storage = multer.diskStorage({
  destination: (req, res, cb) => {
    cb(null, "public/images");
  },
  filename: (req, res, cb) => {
    const uniqueSuffix = Date.now() + path.extname(res.originalname);
    cb(null, uniqueSuffix);
  },
});
const upload = multer({ storage: storage });

const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

router.get("/", async (req, res) => {
  const cars = (await axios.get(`http://localhost:8000/api`)).data;
  let valueCars = cars.map((car) => {
    let day = new Date(car.updatedAt).getDay();
    let month = months[new Date(car.updatedAt).getMonth()];
    let year = new Date(car.updatedAt).getFullYear();
    let hour = new Date(car.updatedAt).getHours();
    let minute = new Date(car.updatedAt).getMinutes();
    return { ...car, day, month, year, hour, minute };
  });

  const insertMessage = req.flash("insertMessages");
  const deleteMessage = req.flash("deleteMessages");

  const data = {
    title: "Home",
    layout: "layouts/main_layout",
    parentLink: "Cars",
    childLink: null,
    breadcrumb: "List Cars",
    cars: valueCars,
    messages: { insertMessage, deleteMessage },
  };
  res.render("index", data);
});

router.get("/filter", async (req, res) => {
  const cars = (await axios.get(`http://localhost:8000/api?filter=${req.query.filter}`)).data;
  let valueCars = cars.map((car) => {
    let day = new Date(car.updatedAt).getDay();
    let month = months[new Date(car.updatedAt).getMonth()];
    let year = new Date(car.updatedAt).getFullYear();
    let hour = new Date(car.updatedAt).getHours();
    let minute = new Date(car.updatedAt).getMinutes();
    return { ...car, day, month, year, hour, minute };
  });
  const data = {
    title: "Filter Car",
    layout: "layouts/main_layout",
    parentLink: "Cars",
    childLink: "List Cars",
    breadcrumb: "Filter",
    cars: valueCars,
    filter: req.query.filter,
  };
  res.render("filter", data);
});

router.get("/search", async (req, res) => {
  if (req.query.q === "") {
    return res.redirect("/");
  }
  const cars = (await axios.get(`http://localhost:8000/api?q=${req.query.q}`)).data;
  let valueCars = cars.map((car) => {
    let day = new Date(car.updatedAt).getDay();
    let month = months[new Date(car.updatedAt).getMonth()];
    let year = new Date(car.updatedAt).getFullYear();
    let hour = new Date(car.updatedAt).getHours();
    let minute = new Date(car.updatedAt).getMinutes();
    return { ...car, day, month, year, hour, minute };
  });
  const data = {
    title: "Search Car",
    layout: "layouts/main_layout",
    cars: valueCars,
    searchQuery: req.query.q,
  };
  res.render("search", data);
});

router.get("/edit/:id", async (req, res) => {
  const car = (await axios.get(`http://localhost:8000/api/${req.params.id}`)).data;
  const data = {
    title: "Edit Car",
    layout: "layouts/main_layout",
    parentLink: "Cars",
    childLink: "List Cars",
    breadcrumb: "Update Car Information",
    car,
  };
  res.render("edit", data);
});

router.get("/create", async (req, res) => {
  const data = {
    title: "Add New Car",
    parentLink: "Cars",
    childLink: "List Cars",
    breadcrumb: "Add New Car",
    layout: "layouts/main_layout",
  };
  res.render("create", data);
});

router.post("/add", upload.single("image"), async (req, res) => {
  switch (req.body.size) {
    case "Small":
      req.body.type = "Sports Car";
      break;
    case "Medium":
      req.body.type = "Medium Car";
      break;
    case "Large":
      req.body.type = "Trail Car";
      break;
  }
  const car = {
    name: req.body.name,
    type: req.body.type,
    price: req.body.price,
    image: req.file.filename,
    size: req.body.size,
  };
  await axios.post(`http://localhost:8000/api`, car);
  req.flash("insertMessages", "Data Berhasil Disimpan");
  res.redirect("/");
});

router.put("/update/:id", upload.single("image"), async (req, res) => {
  const updatedCar = (await axios.get(`http://localhost:8000/api/${req.params.id}`)).data;
  fs.unlink(publicDirectory + `/images/${updatedCar.image}`, () => {
    console.log("Image deleted");
  });
  switch (req.body.size) {
    case "Small":
      req.body.type = "Sports Car";
      break;
    case "Medium":
      req.body.type = "Home Car";
      break;
    case "Large":
      req.body.type = "SUV";
      break;
  }
  const car = {
    name: req.body.name,
    type: req.body.type,
    price: req.body.price,
    image: req.file.filename,
    size: req.body.size,
  };
  await axios.put(`http://localhost:8000/api/${req.params.id}`, car);
  res.redirect("/");
});

router.delete("/delete/:id", async (req, res) => {
  // Delete Car Image
  const car = (await axios.get(`http://localhost:8000/api/${req.params.id}`)).data;
  fs.unlink(publicDirectory + `/images/${car.image}`, () => {
    console.log("Image deleted");
  });
  await axios.delete(`http://localhost:8000/api/${req.params.id}`);
  req.flash("deleteMessages", "Data Berhasil Dihapus");
  res.redirect("/");
});

module.exports = router;
