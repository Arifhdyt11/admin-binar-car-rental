const express = require("express");
const carsRoute = require("./routes/cars");
const app = express();
const port = 8000;

app.use(express.json());

app.use("/api", carsRoute);

app.listen(port, () => {
  console.log(`Server running on http://localhost:${port}`);
});
